`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    03:57:17 06/03/2015 
// Design Name: 
// Module Name:    clockGenerator 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module clockGenerator(
	output clk
);

	reg reg_clk = 1'b0;
	initial 
	begin 
		while(1)
		begin	
			#1 reg_clk = 1'b0;
			#1 reg_clk = 1'b1;
		end
	end	

	assign clk = reg_clk;
endmodule 


