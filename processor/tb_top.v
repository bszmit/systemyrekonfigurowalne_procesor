`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   01:56:33 06/10/2015
// Design Name:   top
// Module Name:   /home/bszmit/IT/Verilog/SR/rozdz_14/processor/tb_top.v
// Project Name:  processor
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: top
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tb_top;

	// Outputs
	wire clk100;

	clockGenerator clockGenerator(
		.clk(clk100)
	);
	reg [7:0] switch = 8'b10101010;
	wire [7:0] led_wire;
	
	wire clk;

	clock_divider #( .div(100) ) 
	clock_divider (
		.clk(clk100),
		.div_clk(clk)
	);

	// Instantiate the Unit Under Test (UUT)
	top uut (
		.clk100(clk),
		.SW(switch),
		.LED(led_wire)
	);

	initial begin
		// Initialize Inputs

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

