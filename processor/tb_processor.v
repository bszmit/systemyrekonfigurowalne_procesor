`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   03:36:56 06/03/2015
// Design Name:   processor
// Module Name:   /home/bszmit/IT/Verilog/SR/rozdz_13/processor/tb_processor.v
// Project Name:  processor
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: processor
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////



module tb_processor;

	// Inputs
	wire clk;

	clockGenerator clockGenerator(
		.clk(clk)
	);

	reg  [7:0] gpi = 8'b10101010;
	wire [7:0] gpo;
	// Instantiate the Unit Under Test (UUT)
	processor uut (
		.clk(clk),
		.gpi(gpi),
		.gpo(gpo)
	);

	initial begin
		// Initialize Inputs

		// Wait 100 ns for global reset to finish
		#100;

        
		// Add stimulus here

	end
      
endmodule

