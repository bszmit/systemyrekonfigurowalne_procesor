`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    01:21:45 06/10/2015 
// Design Name: 
// Module Name:    top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module clock_divider #( parameter div = 100 ) (
	input clk,
	output div_clk
);
	reg [$clog2(div)-1:0] counter = 0;

	always @(posedge clk)
	begin
		counter <= counter + 1;
	end

	assign div_clk = counter[$clog2(div)-1];

endmodule



module top(
	input clk100,

	input [7:0] SW,
	output [7:0] LED
    );


	wire clk;

	clock_divider #( .div(100) ) 
	clock_divider (
		.clk(clk100),
		.div_clk(clk)
	);

	processor processor (
	.clk(clk100),

	.gpi(SW),
	.gpo(LED)
    );



endmodule
