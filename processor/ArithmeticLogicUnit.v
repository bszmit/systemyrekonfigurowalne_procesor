`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    03:47:10 06/03/2015 
// Design Name: 
// Module Name:    ArithmeticLogicUnit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ArithmeticLogicUnit(
	input [7:0]rx,
	input [7:0]ry, 
	output [7:0]arithmeticSum,
	output [7:0]logicAnd,
	output [7:0]logicComparisonToZero
    );

	adder adder(
	  .a(rx), // input [7 : 0] a
	  .b(ry), // input [7 : 0] b
	  .s(arithmeticSum) // output [7 : 0] s
	);

	assign logicAnd = rx & ry;
	assign logicComparisonToZero = rx == 0 ? {7'b0, 1'b1} : {7'b0, 1'b0};


endmodule
