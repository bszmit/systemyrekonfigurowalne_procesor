movi R4, 0x01 ; wlacz diode LED0 (na 1 sekunde)
movi R0, 155 ; musimy doliczyc do 100 (Swiecimy LED0 przez 1 sekunde (takt: 100Hz))
addi R0, R0, 0x01 
jnz R0, 0x02
movi R4, 0x00 ; wylacz diode LED0 ------------------1end---------------------------
movi R4, 0x02 ; zapal diode LED1 i czekaj na przelaczenie przelacznika SW0
andi R0, R5, 0x01 ; R5 to stan przelacznikow SW
jz R0, 0x06 ; czekaj az przelaczysz SW0
movi R4, 0x00 ; wylacz LED1 ------------------------2end-----------------------------
movi R4, 0x03 ; wlacz LED2 (na 1 sekunde)
movi R0, 155 ; musimy doliczyc do 100 (Swiecimy LED2 przez 1 sekunde (takt: 100Hz))
addi R0, R0, 0x01 ; a tot tez jest komentarz
jnz R0, 0x0B ; idz pod adres 11
movi R4, 0x00 ; wylacz LED2 ------------------------3end-----------------------------
movi R4, 0x04 ; zapal diode LED3 i czekaj na przelaczenie przelacznika SW1
andi R0, R5, 0x02 ; R5 to stan przelacznikow SW
jz R0, 0x0F ; czekaj az przelaczysz SW1 (adres 15)
movi R4, 0x00 ; wylacz LED3
movi R2, 0xFF 
movi R3, 0xFF
movi R2, 0x00 
movi R3, 0x00
jumpi 0x00


