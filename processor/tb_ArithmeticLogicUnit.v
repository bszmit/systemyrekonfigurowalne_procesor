`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   03:56:35 06/03/2015
// Design Name:   ArithmeticLogicUnit
// Module Name:   /home/bszmit/IT/Verilog/SR/rozdz_13/processor/tb_ArithmeticLogicUnit.v
// Project Name:  processor
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ArithmeticLogicUnit
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tb_ArithmeticLogicUnit;

	// Inputs
	reg [7:0] rx;
	reg [7:0] ry;

	// Outputs
	wire [7:0] arithmeticSum;
	wire logicAnd;
	wire logicComparisonToZero;

	// Instantiate the Unit Under Test (UUT)
	ArithmeticLogicUnit uut (
		.rx(rx), 
		.ry(ry), 
		.arithmeticSum(arithmeticSum), 
		.logicAnd(logicAnd), 
		.logicComparisonToZero(logicComparisonToZero)
	);

	initial begin
		// Initialize Inputs
		rx = 0;
		ry = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

