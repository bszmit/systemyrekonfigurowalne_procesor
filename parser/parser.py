#!/usr/bin/python3
__author__ = 'bszmit'


class ParserBase:
    def __init__(self, filename):
        self.asmFile = open(filename, 'r')
        self.mcFile = open(self.mcfilename_from_asmfilename(filename), 'w')

    def mcfilename_from_asmfilename(self, filename):
        """
        :type filename: str
        :return:
        """
        return filename.split('.')[0] + '.mc'


class Parser(ParserBase):
    def __init__(self, filename):
        super(Parser, self).__init__(filename)

    def parse(self):
        for line in self.asmFile:
            line = line.rstrip()
            for (command, foo) in zip(self.commands.keys(), self.commands.values()):
                if line.find(command) == -1: continue
                self.mcFile.write('{0:032b}'.format(foo.__func__(line)))
                self.mcFile.write('\n')

        self.asmFile.close()
        self.mcFile.close()

    @staticmethod
    def parse_nop(line):
        """
        0x00 00 06 00
        :param line:
        :return:
        """
        return 0x00000600

    @staticmethod
    def parse_jump(line):
        """
        0x01 36 Rx6 00
        :type line: str
        :return:
        """
        number = 0x01360600
        args = line.split(sep=' ')[1:]
        number += int(args[0][1]) * pow(4, 6)
        return number

    @staticmethod
    def parse_jumpi(line):
        """
        0x01 36 86 imm
        :param line: str
        :return:
        """
        number = 0x01368600
        args = line.split(sep=' ')[1:]
        number += int(args[0], 0)
        return number

    @staticmethod
    def parse_jz(line):
        """
        0x 01 3Rx 86 imm
        :param line: str
        :return:
        """
        number = 0x01308600
        args = line.split(sep=' ')[1:]
        number += int(args[0][1]) * pow(4, 8)
        number += int(args[1], 0)
        return number

    @staticmethod
    def parse_jnz(line):
        """
        0x 02 3Rx 86 imm
        :type line: str
        :return:
        """
        number = 0x02308600
        args = line.split(sep=' ')[1:]
        number += int(args[0][1]) * pow(4, 8)
        number += int(args[1], 0)
        return number

    @staticmethod
    def parse_add(line):
        """
        0x 00 1Rx Ry Rd 00
        :type line: str
        :return:
        """
        number = 0x00100000
        args = line.split(sep=' ')[1:]
        number += int(args[0][1]) * pow(4, 4)
        number += int(args[1][1]) * pow(4, 8)
        number += int(args[2][1]) * pow(4, 6)
        return number

    @staticmethod
    def parse_addi(line):
        """
        0x00 1Rx 8Rd imm
        :type line: str
        :return:
        """
        number = 0x00108000
        args = line.split(sep=' ')[1:]
        number += int(args[0][1]) * pow(4, 4)
        number += int(args[1][1]) * pow(4, 8)
        number += int(args[2], 0)
        return number

    @staticmethod
    def parse_and(line):
        """
        0x 00 0Rx RyRd 00
        :param line:
        :return:
        """
        number = 0x00000000
        args = line.split(sep=' ')[1:]
        number += int(args[0][1]) * pow(4, 4)
        number += int(args[1][1]) * pow(4, 8)
        number += int(args[2][1]) * pow(4, 6)
        return number

    @staticmethod
    def parse_andi(line):
        """
        0x00 0Rx 8Rd imm
        :type line: str
        :return:
        """
        number = 0x00008000
        args = line.split(sep=' ')[1:]
        number += int(args[0][1]) * pow(4, 4)
        number += int(args[1][1]) * pow(4, 8)
        number += int(args[2], 0)
        return number

    @staticmethod
    def parse_load(line):
        """
        0x00 1Rx 6(8+Rd) 00
        :type line: str
        :return:
        """
        number = 0x00106800
        args = line.split(sep=' ')[1:]
        number += int(args[0][1]) * pow(4, 4)
        number += int(args[1][1]) * pow(4, 8)
        return number

    @staticmethod
    def parse_loadi(line):
        """
        0x 00 30 8(8+Rd) imm
        :type line: str
        :return:
        """
        number = 0x00308800
        args = line.split(sep=' ')[1:]
        number += int(args[0][1]) * pow(4, 4)
        number += int(args[1], 0)
        return number

    @staticmethod
    def parse_mov(line):
        """
        0x00 1Rx 6Rd 00
        :type line: str
        :return:
        """
        number = 0x00106000
        args = line.split(sep=' ')[1:]
        number += int(args[0][1]) * pow(4, 4)
        number += int(args[1][1]) * pow(4, 8)
        return number

    @staticmethod
    def parse_movi(line):
        """
        0x00 16 8Rd imm
        :type line: str
        :return:
        """
        number = 0x00168000
        args = line.split(sep=' ')[1:]
        number += int(args[0][1]) * pow(4, 4)
        number += int(args[1], 0)
        return number

    commands = {
        'nop': parse_nop,
        'jump ': parse_jump,
        'jumpi ': parse_jumpi,
        'jz ': parse_jz,
        'jnz ': parse_jnz,
        'add ': parse_add,
        'addi ': parse_addi,
        'and ': parse_and,
        'andi ': parse_andi,
        'load ': parse_load,
        'loadi ': parse_loadi,
        'mov ' : parse_mov,
        'movi ' : parse_movi
    }


import sys

if __name__ == '__main__':
    if len(sys.argv) == 1:
        sys.stderr.write('ERR: no filename given\n')
        exit(1)

    filename = sys.argv[1]
    filename = filename.split('.')[0]
    try:
        p = Parser(filename+'.asm')
    except FileNotFoundError as err:
        sys.stderr.write('ERR: file cannot be found\n')
        exit(1)

    p.parse()

    file = open(filename+'.mc')
    outFile = open(filename+'hex.mc', 'w')
    i = 0
    for line in file:
        outFile.write('instructions[%d] = 32\'h' % i)
        outFile.write('{0:08x}'.format(int(line, 2)))
        outFile.write(';\n')
        i += 1

    file.close()
    outFile.close()




