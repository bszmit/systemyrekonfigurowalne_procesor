`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   04:00:48 06/03/2015
// Design Name:   multiplexer
// Module Name:   /home/bszmit/IT/Verilog/SR/rozdz_13/processor/tb_multiplexer.v
// Project Name:  processor
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: multiplexer
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tb_multiplexer;

	// Inputs
	reg [31:0] x;
	reg [1:0] a;

	// Outputs
	wire [7:0] y;

	// Instantiate the Unit Under Test (UUT)
	multiplexer uut (
		.x(x), 
		.a(a), 
		.y(y)
	);

	initial begin
		// Initialize Inputs
		x = 0;
		a = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

