`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    04:03:31 06/03/2015 
// Design Name: 
// Module Name:    registerBank 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module registerBank(
	input clk,
	input [7:0] rd,	//data input
	input [7:0] ce,	//wejscie CE. if == 1, mozliwosc zapisu.
	input [7:0] r7,


	output [8*8-1:0] out,	//serialized {r0, r1, ...}

	output [7:0] pc_addr,

	input  [7:0] gpi,
	output [7:0] gpo
    );

	reg [7:0]bank [7:0]; //osiem 8-bitowych rejestrow 
	wire [7:0]wire_bank [7:0];

	genvar g;
	generate
		for (g = 0; g < 8; g=g+1) begin
			assign wire_bank[g] = bank[g];
		end
	endgenerate


	integer k;
	initial
	begin
		for (k = 0; k < 8; k = k + 1)
		begin
			bank[k] <= 8'b0;
		end
	end

	integer i;
	always @(posedge clk)
	begin
		for (i = 0; i < 6; i=i+1) begin
			if (ce[i] == 1'b1) begin
				bank[i] <= rd;
			end
		end
		bank[6] <= 0;
		bank[7] <= r7;
	end

	assign pc_addr = bank[7];

	assign out = {bank[7], bank[6], gpi, bank[4], bank[3], bank[2], bank[1], bank[0]};

	assign gpo = bank[4];

endmodule
