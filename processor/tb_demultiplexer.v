`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   04:43:51 06/03/2015
// Design Name:   demultiplexer
// Module Name:   /home/bszmit/IT/Verilog/SR/rozdz_13/processor/tb_demultiplexer.v
// Project Name:  processor
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: demultiplexer
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tb_demultiplexer;

	// Inputs
	reg x;
	reg [2:0] a;

	// Outputs
	wire [6:0] y;

	// Instantiate the Unit Under Test (UUT)
	demultiplexer uut (
		.x(1'b1), 
		.a(a), 
		.y(y)
	);

	initial begin
		// Initialize Inputs
		x = 0;
		a = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

