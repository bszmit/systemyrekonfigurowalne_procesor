`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   04:22:20 06/03/2015
// Design Name:   registerBank
// Module Name:   /home/bszmit/IT/Verilog/SR/rozdz_13/processor/tb_registerBank.v
// Project Name:  processor
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: registerBank
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tb_registerBank;

	// Inputs
	wire clk;
	reg [7:0] rd;
	reg [7:0] ce;

	// Outputs
	wire [63:0] out;

	clockGenerator clockGenerator (
		.clk(clk)
	);

	// Instantiate the Unit Under Test (UUT)
	registerBank uut (
		.clk(clk), 
		.rd(rd), 
		.ce(ce), 
		.out(out)
	);

	initial begin
		// Initialize Inputs

		ce = 8'b00000000;
		rd = 8'd0;
		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

