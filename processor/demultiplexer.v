`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    04:37:04 06/03/2015 
// Design Name: 
// Module Name:    demultiplexer 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module demultiplexer(
	input x,
	input [2:0] a,	//wejscia indeksujace
	output [6:0]y	//bo bez ostatniego rejestru
    );

	genvar i;
	generate
		for (i=0; i < 7; i=i+1)
		begin
			assign y[i] = a == i ? x : 0;
		end
	endgenerate
	//assign  y[0] = a == 2'b000 ? x:0


endmodule
