`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    03:35:46 06/03/2015 
// Design Name: 
// Module Name:    multiplexer 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module multiplexer#( parameter x_size = 4 , parameter a_size = 2) 
	(
	input [x_size*8-1:0]x,
	input [a_size-1:0]a,
	output [7:0]y
    );
	wire [7:0] tab[x_size-1:0];

	genvar i;
	generate 
		//for (i=x_size-1; i>=0; i=i-1)
		for (i=0; i < x_size; i=i+1)
		begin
			assign tab[i] = x[8*(i+1)-1:8*i]; 
		end
	endgenerate

	assign y = tab[a];

endmodule
