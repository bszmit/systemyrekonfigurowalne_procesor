`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    03:34:47 06/03/2015 
// Design Name: 
// Module Name:    processor 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module parseMachineCode (
	input clk,
	input [7:0] pc_addr,

	output [1:0] pc_op,
	output [1:0] alu_op,
	output [2:0] rx_op,
	output		 imm_op,
	output [2:0] ry_op,
	output       rd_op,
	output [2:0] d_op,
	output [7:0] imm
);
	reg [1:0] pc_op_reg  = 0;
	reg [1:0] alu_op_reg = 0;
	reg [2:0] rx_op_reg  = 0;
	reg       imm_op_reg = 0;
	reg [2:0] ry_op_reg  = 0;
	reg       rd_op_reg  = 0;
	reg [2:0] d_op_reg   = 0;
	reg [7:0] imm_reg    = 0;

	

//	assign pc_op  = pc_op_reg;
//	assign alu_op = alu_op_reg;
//	assign rx_op  = rx_op_reg;
//	assign imm_op = imm_op_reg;
//	assign ry_op  = ry_op_reg;
//	assign rd_op  = rd_op_reg;
//	assign d_op   = d_op_reg;
//	assign imm    = imm_reg;

	reg [31:0] instructions [255:0];

	// {001, Rx, 6, Rd, 00} - mov Rd, Rx
	// {00168, Rd, imm} - movi Rd,imm 

	// 0x 00 00 06 00 - nop

	// 0x 01 36 Rx6 00 - jump Rx - skacze na wartosc spod Rx
	// 0x 01 36 86 imm - jumpi imm - skacze na wartosc spod Rx
	// 0x 01 3Rx 86 imm - jz Rx,imm - jesli Rx == 0, skacze pod adres imm
	// 0x 02 3Rx 86 imm - inz Rx, imm - jesli Rx != 0, skacze pod adres imm

	// 0x 00 1Rx RyRd 00 - add Rd, Rx, Ry - wynik sumy Rx i Ry jest zapisywany do Rd
	// 0x 00 1Rx 8Rd imm - addi Rd, Rx, imm - wynik wumy Rx i imm jest zapisywany do Rd
	// 0x 00 0Rx RyRd 00 - and Rd, Rx, Ry - wynik operacji logicznej AND na Rx i Ry zapisywany do Rd
	// 0x 00 0Rx 8Rd imm - andi Rd, Rx, imm - wynik operacji logicznej AND na Rx i imm zapisywany do Rd
	// 0x 00 1Rx 6(8+Rd) 00 - load Rd, Rx - zaladuj do Rd wartosci pamieci danych spod adresu Rx
	// 0x 00 30 8(8+Rd) imm - loadi Rd, imm - zaladuj do rejestru Rd wartosc z pamieci danych spod adresu imm
	initial begin
		/*
		instructions[0] = 32'h00168000;
		instructions[1] = 32'h00168104;
		instructions[2] = 32'h00168001;
		instructions[3] = 32'h00001200;
		instructions[4] = 32'h01328602;
		instructions[5] = 32'h00168301;
		*/
	   /*
		instructions[0] = 32'h00156000;
		instructions[1] = 32'h00156200;
		instructions[2] = 32'h00156400;
		*/
	   /*
		instructions[0] = 32'h00168401;
		instructions[1] = 32'h00168064;
		instructions[2] = 32'h02308601;
		instructions[3] = 32'h00168400;
		instructions[4] = 32'h00168402;
		*/
	   /*
	   instructions[0] = 32'h00168401;
instructions[1] = 32'h00168451;
instructions[2] = 32'h00168064;
instructions[3] = 32'h02308601;
instructions[4] = 32'h00168400;
instructions[5] = 32'h00168402;
*/
/*
instructions[0] = 32'h00168401;
instructions[1] = 32'h00168064;
instructions[2] = 32'h02308602;
instructions[3] = 32'h001684f0;
instructions[4] = 32'h00168402;
*/
/*
instructions[0] = 32'h00168401;
instructions[1] = 32'h0016809b;
instructions[2] = 32'h00108001;
instructions[3] = 32'h02308602;
instructions[4] = 32'h001684f0;
instructions[5] = 32'h00168402;
*/
/*
instructions[0] = 32'h00168000;
instructions[1] = 32'h00168104;
instructions[2] = 32'h00108001;
instructions[3] = 32'h00001200;
instructions[4] = 32'h01328602;
instructions[5] = 32'h00168301;
*/
instructions[0] = 32'h00168401;
instructions[1] = 32'h0016809b;
instructions[2] = 32'h00108001;
instructions[3] = 32'h02308602;
instructions[4] = 32'h00168400;
instructions[5] = 32'h00168402;
instructions[6] = 32'h00058001;
instructions[7] = 32'h01308606;
instructions[8] = 32'h00168400;
instructions[9] = 32'h00168403;
instructions[10] = 32'h0016809b;
instructions[11] = 32'h00108001;
instructions[12] = 32'h0230860b;
instructions[13] = 32'h00168400;
instructions[14] = 32'h00168404;
instructions[15] = 32'h00058002;
instructions[16] = 32'h0130860f;
instructions[17] = 32'h00168400;
instructions[18] = 32'h001682ff;
instructions[19] = 32'h001683ff;
instructions[20] = 32'h00168200;
instructions[21] = 32'h00168300;
instructions[22] = 32'h01368600;







	end
	
//	always @(posedge clk)
//	begin
//		pc_op_reg  <= instructions[pc_addr][25:24];
//		alu_op_reg <= instructions[pc_addr][21:20];
//		rx_op_reg  <= instructions[pc_addr][18:16];
//		imm_op_reg <= instructions[pc_addr][15];
//		ry_op_reg  <= instructions[pc_addr][14:12];
//		rd_op_reg  <= instructions[pc_addr][11];
//		d_op_reg   <= instructions[pc_addr][10:8];
//		imm_reg    <= instructions[pc_addr][7:0];
//	end
//	

	assign	pc_op  = instructions[pc_addr][25:24];
	assign	alu_op = instructions[pc_addr][21:20];
	assign	rx_op  = instructions[pc_addr][18:16];
	assign	imm_op = instructions[pc_addr][15];
	assign	ry_op  = instructions[pc_addr][14:12];
	assign	rd_op  = instructions[pc_addr][11];
	assign	d_op   = instructions[pc_addr][10:8];
	assign	imm    = instructions[pc_addr][7:0];
endmodule 

module dataMemory_mod (
	input [7:0] alu_res,

	output [7:0] dataMemory
);
	
	reg [7:0] data_reg [255:0];

	assign dataMemory = data_reg[alu_res];

	initial begin
		data_reg[0] = 8'b11110000;
		data_reg[1] = 8'b00001111;
		data_reg[3] = 8'b10101010;
		data_reg[4] = 8'b11001100;
	end

endmodule


module processor(
	input clk,

	input [7:0] gpi,
	output [7:0] gpo
    );

	wire [1:0] pc_op      ;
	wire [1:0] alu_op     ;
	wire [2:0] rx_op      ;
	wire       imm_op     ;
	wire [2:0] ry_op      ;
	wire       rd_op      ;
	wire [2:0] d_op       ;
	wire [7:0] alu_res	  ;
	wire [7:0] dataMemory ;
	wire [7:0] imm        ;
	wire [7:0] pc_addr    ;


	wire warunekSkoku;
	assign warunekSkoku = pc_op == 2'b00 ? 1'b0 : 
						( pc_op == 2'b01 && ALUcomparison ? 1'b1 : 
						( pc_op == 2'b10 && ! ALUcomparison ? 1'b1 : 1'b0 ) );

	parseMachineCode parseMachineCode(
		.clk(clk),
		.pc_addr(pc_addr),

		.pc_op(pc_op),
		.alu_op(alu_op),
		.rx_op(rx_op),
		.imm_op(imm_op),
		.ry_op(ry_op),
		.rd_op(rd_op),
		.d_op(d_op),
		.imm(imm)
	);

	dataMemory_mod dataMemory_mod (
		.alu_res(alu_res),
		.dataMemory(dataMemory)
	);
	

	////////////////////////////////////////////////////////////////////////////
	/////////// DEKODER 
	////////////////////////////////////////////////////////////////////////////
	wire [6:0] registerBankEnable;
	demultiplexer dekoder (
		.x(1'b1), 
		.a(d_op),	//[2:0]
		.y(registerBankEnable)	//[6:0]
	);


	////////////////////////////////////////////////////////////////////////////
	/////////// BANK REJESTROW
	////////////////////////////////////////////////////////////////////////////
	wire [7:0]pc_mux_out;
	wire [7:0]r7_out;
	wire [7:0]rd_out;
	wire [8*8-1:0]serializedRegBank;

	registerBank registerBank (
		.clk(clk),
		.rd(rd_out),
		.ce({1'b0, registerBankEnable}),
		.r7(pc_mux_out),
		.pc_addr(pc_addr),
		.out(serializedRegBank),

		.gpi(gpi),
		.gpo(gpo)
	);

	wire [7:0] regBank [7:0];
	
	genvar i;
	generate
		for (i=0; i < 8; i=i+1) begin
			assign regBank[i] = serializedRegBank[8*(i+1)-1:8*i];
		end
	endgenerate
	

	////////////////////////////////////////////////////////////////////////////
	/////////// RD MUX
	////////////////////////////////////////////////////////////////////////////
	multiplexer #( .x_size(2), .a_size(1) )
		rd_mux (
			.x({dataMemory, alu_res}),
			.a(rd_op),
			.y(rd_out)
		);


	////////////////////////////////////////////////////////////////////////////
	/////////// RX MUX
	////////////////////////////////////////////////////////////////////////////
	wire [7:0] rx_out;
	multiplexer #( .x_size(8), .a_size(3) )
		rx_mux (
			.x(serializedRegBank),
			.a(rx_op),
			.y(rx_out)
		);

	////////////////////////////////////////////////////////////////////////////
	/////////// RY MUX
	////////////////////////////////////////////////////////////////////////////
	wire [7:0] ry_out;
	multiplexer #( .x_size(8), .a_size(3) )
		ry_mux (
			.x(serializedRegBank),
			.a(ry_op),
			.y(ry_out)
		);


	////////////////////////////////////////////////////////////////////////////
	/////////// IMM MUX
	////////////////////////////////////////////////////////////////////////////
	wire [7:0] imm_mux_out;
	multiplexer #( .x_size(2), .a_size(1) )
		imm_mux (
			.x({imm, ry_out}),
			.a(imm_op),
			.y(imm_mux_out)
		);

	
	////////////////////////////////////////////////////////////////////////////
	/////////// ALU - Arithmetic Logic Unit
	////////////////////////////////////////////////////////////////////////////
	wire [7:0] ALUsum;
	wire [7:0] ALUlogic;
	wire [7:0] ALUcomparison;
	ArithmeticLogicUnit ArithmeticLogicUnit (
		.rx(rx_out),
		.ry(imm_mux_out),
		.arithmeticSum(ALUsum),
		.logicAnd(ALUlogic),
		.logicComparisonToZero(ALUcomparison)
	);


	////////////////////////////////////////////////////////////////////////////
	/////////// ALU MUX
	////////////////////////////////////////////////////////////////////////////
	multiplexer #( .x_size(4), .a_size(2) )
		alu_mux (
			.x({imm_mux_out, ALUcomparison, ALUsum, ALUlogic}),
			.a(alu_op),
			.y(alu_res)
		);


	////////////////////////////////////////////////////////////////////////////
	/////////// PC MUX
	////////////////////////////////////////////////////////////////////////////
	wire [7:0]pc_addr_plus_1;
	assign pc_addr_plus_1 = pc_addr + 1;
	multiplexer #( .x_size(2), .a_size(1) )
		pc_mux (
			.x({alu_res, pc_addr_plus_1}),
			.a(warunekSkoku),
			.y(pc_mux_out)
		);

endmodule



